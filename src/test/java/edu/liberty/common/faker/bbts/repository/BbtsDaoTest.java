package edu.liberty.common.faker.bbts.repository;

import edu.liberty.common.faker.bbts.FakerBbtsEnvironmentId;
import edu.liberty.common.faker.bbts.config.TestConfig;
import edu.liberty.common.faker.bbts.domain.Card;
import edu.liberty.common.faker.bbts.domain.Customer;
import static edu.liberty.common.faker.bbts.util.Util.convertDateToBbtsDate;
import static edu.liberty.common.faker.bbts.util.Util.luidToCardNumber;
import edu.liberty.common.faker.core.FakePerson;
import edu.liberty.common.faker.core.service.FakerService;
import edu.liberty.common.faker.core.service.PersonFakerService;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ddavenport3
 * 
 * Since Customer and Card have a circular reference, CardDao and 
 * CustomerDao have to be tested at the same time.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class BbtsDaoTest extends FakerService {
    
    static Logger logger = Logger.getLogger(BbtsDaoTest.class);

    @Autowired
    private CardDao cardDaoTest;
    @Autowired
    private CustomerDao customerDaoTest;
    @Autowired
    private PersonFakerService personTestService;
    
    private FakePerson fakePerson;
    
    @Before
    public void setUp() {
        fakePerson = new FakePerson();
        personTestService.create(fakePerson); 
    }
    
    @Test
    @Transactional("bbtsTransactionManager")
    public void testCustomerDao() {
                 

        Customer customer = createCustomer(fakePerson);
        
        customer = customerDaoTest.save(customer);
        Long customerId = customer.getCustomerId();
        flush();
        
        customer = customerDaoTest.findByCustomerId(customerId);
        assertNotNull(customer);
        assertEquals("Testy", customer.getFirstName());
        assertEquals("Testerson", customer.getLastName());
        assertEquals("T", customer.getIsActive());
        assertEquals("M", customer.getSex());
        assertEquals(Integer.valueOf(1234), customer.getPinNumber());

    }
    
    private Customer createCustomer(FakePerson fakePerson) {
        
        Customer customer = new Customer(); 
        
        customer.setCardNumber(null);
        customer.setCustomerNumber(luidToCardNumber(fakePerson.getStudentId()));
        
        customer.setFirstName(fakePerson.getFirstName());
        customer.setMiddleName(fakePerson.getMiddle());
        customer.setLastName(fakePerson.getLastName());
        customer.setBirthDate(convertDateToBbtsDate(new Date(System.currentTimeMillis()))); 
        
        // required fields
        customer.setIsActive("T");
        customer.setLastModDateTime(40267.0);
        customer.setDoorExtendedUnlockAllowed("F");
        customer.setSex("M");
        customer.setPinNumber(1234);
        return customer;
    }
    
    @Test
    @Transactional("bbtsTransactionManager")
    public void testCardDao() {
        
        String cardNumber = luidToCardNumber(fakePerson.getStudentId());
        Card card = createCard();
        card.setCardNumber(cardNumber);
        cardDaoTest.save(card);
        flush();

        card = cardDaoTest.findByCardNumber(cardNumber);
        assertNotNull(card);
        assertEquals(Integer.valueOf(0), card.getCardType());
        assertEquals(Integer.valueOf(2), card.getCardStatus());
        assertEquals("F", card.getLostFlag());
        assertEquals(Integer.valueOf(0), card.getIssuerId());
        
    }
    
    private Card createCard() {
        Card card = new Card();  
        
        // required fields
        card.setCustomerId(null);
        card.setCardType(0);
        card.setCardStatus(2);
        card.setLostFlag("F");
        card.setDomainId(cardDaoTest.getNewDomainId());
        card.setIssuerId(0); 
        card.setCreatedDateTime(new Date(System.currentTimeMillis()));
        card.setModifiedDateTime(new Date(System.currentTimeMillis()));
        return card;
    }
    
    @Override
    @PersistenceContext(unitName = "bbts")
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }
    
    public String getEnvironmentId() {
        return FakerBbtsEnvironmentId.ENVIRONMENT_ID;
    }
    
    @Configuration
    @Import({TestConfig.class})
    protected static class Config {}

    
}
