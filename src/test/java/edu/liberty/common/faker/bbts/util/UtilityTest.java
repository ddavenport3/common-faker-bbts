package edu.liberty.common.faker.bbts.util;

import edu.liberty.common.faker.bbts.config.TestConfig;
import static edu.liberty.common.faker.bbts.util.Util.custNumToLuid;
import static edu.liberty.common.faker.bbts.util.Util.luidToCardNumber;
import static edu.liberty.common.faker.bbts.util.Util.convertBbtsDateToDate;
import static edu.liberty.common.faker.bbts.util.Util.convertDateToBbtsDate;
import static java.lang.Math.abs;
import java.util.Date;
import javax.management.timer.Timer;
import org.apache.log4j.Logger;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author ddavenport3
 * 
 * Since Customer and Card have a circular reference CardDao and 
 * CustomerDao have to be tested at the same time.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class UtilityTest {
    
    static Logger logger = Logger.getLogger(UtilityTest.class);
    
    private static final String CUSTNUM_REGEX = "^0{14}\\d{8}$";
    private static final String LUID_REGEX = "^L\\d{8}$";

    @Test
    public void testIdConversion() {

        String startCardNum = "0000000000000012345678";
        String startLuid = custNumToLuid(startCardNum);
        String endCardNum = luidToCardNumber(startLuid);
        String endLuid = custNumToLuid(endCardNum);
                
        assertTrue(endCardNum.matches(CUSTNUM_REGEX));
        assertTrue(endLuid.matches(LUID_REGEX));
        
        assertEquals(startCardNum, endCardNum);
        assertEquals(startLuid, endLuid);
       
    }
    
    @Test
    public void testDateConversion() throws Exception {
        
        // Since conversions are not exact we check them to within 2 milliseconds
        Long diffInMillis = 2L; 
        Double diffInDays = diffInMillis/(double)Timer.ONE_DAY;
        
        Date startDate = new Date(System.currentTimeMillis());
        Double startBbtsDate = convertDateToBbtsDate(startDate);
        Date endDate = convertBbtsDateToDate(startBbtsDate);
        Double endBbtsDate = convertDateToBbtsDate(endDate);

        assertTrue(abs(startBbtsDate - endBbtsDate) < diffInDays);
        assertTrue(abs(startDate.getTime() - endDate.getTime()) < diffInMillis); 
        
    }
    
    @Configuration
    @Import({TestConfig.class})
    protected static class Config {}

}
