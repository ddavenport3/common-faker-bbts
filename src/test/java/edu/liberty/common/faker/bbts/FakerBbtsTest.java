package edu.liberty.common.faker.bbts;

import edu.liberty.common.faker.bbts.service.BbtsCustomerFakerService;
import edu.liberty.common.faker.core.FakePerson;
import edu.liberty.common.faker.core.service.NamedFakerObjectService;
import org.apache.log4j.Logger;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;
import static org.springframework.test.util.ReflectionTestUtils.setField;

/**
 *
 * @author ddavenport3
 */
@RunWith(MockitoJUnitRunner.class)
public class FakerBbtsTest {
    
    private static final Logger logger = Logger.getLogger(FakerBbtsTest.class);
    
    private FakerBbts fakerBbts;
    @Mock
    private NamedFakerObjectService namedFakerObjectService;
    @Mock
    private BbtsCustomerFakerService customerFakerService;
    
    @Before
    public void setUp() {
        
        fakerBbts = new FakerBbts();
        
        // override service injection into FakerBbts
        setField(fakerBbts,"customerFakerService", customerFakerService);
        setField(fakerBbts,"namedFakerObjectService", namedFakerObjectService);
    
    }
    
    @Test
    public void testCreateCustomer() {
        
        logger.info("Running testCreateCustomer");
        
        FakeCustomer fakeCustomer = new FakeCustomer();
        FakeCard fakeCard = new FakeCard(fakeCustomer);
        FakePerson fakePerson = new FakePerson();
        
        customerFakerService.create(fakeCustomer);
        namedFakerObjectService.add(fakeCustomer);
        namedFakerObjectService.add(fakeCard);
        
        fakeCustomer = fakerBbts.createCustomer(fakePerson);
        assertNotNull(fakeCustomer);
        
        verify(customerFakerService, times(1)).create(fakeCustomer);
        verify(namedFakerObjectService, times(1)).add(fakeCustomer);
        verify(namedFakerObjectService, times(1)).add(fakeCard);

    }
    
}
