package edu.liberty.common.faker.bbts.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;
import java.util.UUID;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import edu.liberty.common.faker.banner.service.BannerPersonFakerServiceImpl;
import edu.liberty.common.faker.bbts.service.BbtsCardFakerServiceImpl;
import edu.liberty.common.faker.bbts.service.BbtsCustomerFakerServiceImpl;
import edu.liberty.common.faker.core.service.PersonFakerService;

/**
 * 
 * @author whboyd
 * @author ddavenport3
 * 
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "bbtsEntityManagerFactory", 
        transactionManagerRef = "bbtsTransactionManager",
        basePackages = {"edu.liberty.common.faker.bbts"})
public class TestConfig extends BannerConfig {  
    
    @Bean
    public BbtsCustomerFakerServiceImpl customerTestService() {
        return new BbtsCustomerFakerServiceImpl();
    }
    
    @Bean
    public BbtsCardFakerServiceImpl cardTestService() {
        return new BbtsCardFakerServiceImpl();
    }
    
    @Bean
    public PersonFakerService personTestService() {
        return new BannerPersonFakerServiceImpl();
    }

    @Bean
    public ComboPooledDataSource bbtsDataSource() {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setJdbcUrl("jdbc:oracle:thin:@xdev01-scan.liberty.edu:1521/BBTSQA.liberty.edu");
        dataSource.setUser("ZAPPDEV_VATQA");
        dataSource.setPassword("TM3bz4FHbkzQr59N");
        try {
            dataSource.setDriverClass("oracle.jdbc.OracleDriver");
        } catch (PropertyVetoException e) {
            throw new RuntimeException("it broke", e);
        }
        dataSource.setInitialPoolSize(1);
        dataSource.setMinPoolSize(1);
        dataSource.setMaxPoolSize(3);
        return dataSource;
    }

    @Bean(name = {"bbtsTransactionManager"})
    public PlatformTransactionManager bbtsTransactionManager() {
        return new JpaTransactionManager(bbtsEntityManagerFactory().getObject());
    }

    @Bean(name = {"bbtsEntityManagerFactory"})
    public LocalContainerEntityManagerFactoryBean bbtsEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean managerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        managerFactoryBean.setPersistenceUnitName("bbts");
        managerFactoryBean.setDataSource(bbtsDataSource());
        managerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter());
        managerFactoryBean.setPackagesToScan("edu.liberty.common.faker.bbts");
        Properties props = new Properties();
        props.setProperty("hibernate.ejb.entitymanager_factory_name", managerFactoryBean.getPersistenceUnitName()+UUID.randomUUID());
        props.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
        managerFactoryBean.setJpaProperties(props);
        return managerFactoryBean;
    }
    
}
