package edu.liberty.common.faker.bbts.service;

import edu.liberty.common.faker.banner.service.BannerPersonFakerServiceImpl;
import edu.liberty.common.faker.bbts.config.TestConfig;
import edu.liberty.common.faker.bbts.domain.Card;
import edu.liberty.common.faker.bbts.domain.Customer;
import edu.liberty.common.faker.bbts.FakeCard;
import edu.liberty.common.faker.bbts.FakeCustomer;
import edu.liberty.common.faker.core.FakePerson;
import edu.liberty.common.faker.core.service.PersonFakerService;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;



/**
 *
 * @author ddavenport3 
 * 
 * 
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class BbtsServicelIntegrationTest extends BannerPersonFakerServiceImpl {
    
    static Logger logger = Logger.getLogger(BbtsServicelIntegrationTest.class);

    @Autowired
    private BbtsCustomerFakerService customerTestService;
    @Autowired
    private BbtsCardFakerService cardTestService;
    @Autowired
    private PersonFakerService personTestService;
    
    FakeCustomer fakeCustomer;
    
    @Before
    public void setUp() {         

        FakePerson fakePerson = new FakePerson();
        personTestService.create(fakePerson);
        
        fakeCustomer = new FakeCustomer();
        fakeCustomer.setFakePerson(fakePerson);
        customerTestService.create(fakeCustomer);
        
    }
    
    @Test
    public void testCustomerCreate() throws Exception {

        // test that the fakeCustomer is correctly populated by customerTestService.create(fakeCustomer);
        assertNotNull(fakeCustomer);
        assertEquals("Testy", fakeCustomer.getFirstName());
        assertEquals("Testerson", fakeCustomer.getLastName());
        assertEquals("T", fakeCustomer.getIsActive());
        assertEquals("M", fakeCustomer.getSex());
        assertEquals(Integer.valueOf(1234), fakeCustomer.getPinNumber());
        
        // test that the fakeCard has a fakeCard correctly populated by customerTestService.create(fakeCustomer);
        FakeCard fakeCard = fakeCustomer.getFakeCard();
        assertNotNull(fakeCard);
        assertEquals(Integer.valueOf(0), fakeCard.getCardType());
        assertEquals(Integer.valueOf(2), fakeCard.getCardStatus());
        assertEquals("F", fakeCard.getLostFlag());
        assertEquals(Integer.valueOf(0), fakeCard.getIssuerId());
        
        // Test3: test that the circular reference has been closed
        assertEquals(fakeCard.getCardNumber(), fakeCustomer.getCardNumber());
        assertEquals(fakeCard.getCustomerId(), fakeCustomer.getCustomerId());  
        
    }
    
    @Test
    public void testCardFindByCardNumber() {
        logger.info("Running testCardFindByCardNumber");
        String cardNumber = fakeCustomer.getFakeCard().getCardNumber();
        Card card = cardTestService.findByCardNumber(cardNumber);
        assertNotNull(card);
    }
    
    @Test
    public void testCustomerFindByCustomerId() {
        logger.info("Running testCustomerFindByCustomerId");
        Long customerId = fakeCustomer.getCustomerId();
        Customer customer = customerTestService.findByCustomerId(customerId);
        assertNotNull(customer);
    }
    
    @Configuration
    @Import({TestConfig.class})
    protected static class Config {}

}
