package edu.liberty.common.faker.bbts.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;
import java.util.UUID; 
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author whboyd
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "bannerEntityManagerFactory", 
        transactionManagerRef = "bannerTransactionManager",
        basePackages = {"edu.liberty.common.faker.banner"})
public class BannerConfig {
    
    @Bean
    public HibernateJpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setShowSql(false);
        return jpaVendorAdapter;
    }

    @Bean
    public ComboPooledDataSource bannerDataSource() {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setJdbcUrl("jdbc:oracle:thin:@xdev01-scan.liberty.edu:1521/APPDEV.liberty.edu");
        dataSource.setUser("baninst1");
        dataSource.setPassword("Ipickedit");
        try {
            dataSource.setDriverClass("oracle.jdbc.OracleDriver");
        } catch (PropertyVetoException e) {
            throw new RuntimeException("it broke", e);
        }
        dataSource.setInitialPoolSize(1);
        dataSource.setMinPoolSize(1);
        dataSource.setMaxPoolSize(3);
        return dataSource;
    }

    @Bean(name = {"bannerTransactionManager"})
    public PlatformTransactionManager bannerTransactionManager() {
        return new JpaTransactionManager(bannerEntityManagerFactory().getObject());
    }

    @Bean(name = {"bannerEntityManagerFactory"})
    public LocalContainerEntityManagerFactoryBean bannerEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean managerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        managerFactoryBean.setPersistenceUnitName("banner");
        managerFactoryBean.setDataSource(bannerDataSource());
        managerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter());
        managerFactoryBean.setPackagesToScan("edu.liberty.common.faker.banner");
        Properties props = new Properties();
        props.setProperty("hibernate.ejb.entitymanager_factory_name", managerFactoryBean.getPersistenceUnitName()+UUID.randomUUID());
        props.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
        managerFactoryBean.setJpaProperties(props);
        return managerFactoryBean;
    }
    
}
