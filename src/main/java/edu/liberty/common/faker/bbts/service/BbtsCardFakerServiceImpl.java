package edu.liberty.common.faker.bbts.service;

import edu.liberty.common.faker.bbts.FakerBbtsEnvironmentId;
import edu.liberty.common.faker.bbts.domain.Card;
import edu.liberty.common.faker.bbts.FakeCard;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import edu.liberty.common.faker.bbts.repository.CardDao;
import edu.liberty.common.faker.bbts.FakeCustomer;
import edu.liberty.common.faker.core.service.FakerService;
import java.util.Date;

/**
 *
 * @author ddavenport3
 * 
 */
@Service
public class BbtsCardFakerServiceImpl extends FakerService implements BbtsCardFakerService  {
    
    private static final Logger logger = Logger.getLogger(BbtsCardFakerServiceImpl.class);
    
    @Autowired
    private CardDao cardDao;

    @Override
    @Transactional("bbtsTransactionManager")
    public void create(FakeCustomer fakeCustomer) {

        populateFakeCard(fakeCustomer);
        Card card = populateCard(fakeCustomer);
        cardDao.save(card);
        flush();

    }
    
    private Card populateCard(FakeCustomer fakeCustomer) {
        Card card = new Card();  
        FakeCard fakeCard = fakeCustomer.getFakeCard();
        card.setCardNumber(fakeCard.getCardNumber());  
        card.setCustomerId(null);
        card.setCardType(fakeCard.getCardType());
        card.setCardStatus(fakeCard.getCardStatus());
        card.setLostFlag(fakeCard.getLostFlag());
        card.setIssuerId(fakeCard.getIssuerId()); 
        card.setDomainId(cardDao.getNewDomainId());  
        card.setModifiedDateTime(new Date(System.currentTimeMillis()));
        card.setCreatedDateTime(fakeCard.getCreatedDateTime());
        return card;
    }
    
    private void populateFakeCard(FakeCustomer fakeCustomer) {
        
        FakeCard fakeCard = fakeCustomer.getFakeCard();
        
        // create circular reference between FakeCustomer and FakeCard
        fakeCard.setCardNumber(fakeCustomer.getCardNumber());
        fakeCard.setCustomerId(fakeCustomer.getCustomerId());
        
        if (fakeCard.getCardType() ==  null) fakeCard.setCardType(0);
        if (fakeCard.getLostFlag() ==  null) fakeCard.setLostFlag("F");
        if (fakeCard.getCardStatus() ==  null) fakeCard.setCardStatus(2);
        if (fakeCard.getIssuerId() ==  null) fakeCard.setIssuerId(0); 
        if (fakeCard.getCreatedDateTime() ==  null) fakeCard.setCreatedDateTime(new Date(System.currentTimeMillis()));
        
    }
    
    @Override
    public Card findByCardNumber(String cardNumber) {
        return cardDao.findByCardNumber(cardNumber);
    }
    
    @Override
    @PersistenceContext(unitName = "bbts")
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    @Override
    public String getEnvironmentId() {
        return FakerBbtsEnvironmentId.ENVIRONMENT_ID;
    }
    

}

