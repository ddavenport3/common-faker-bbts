package edu.liberty.common.faker.bbts.repository;

import edu.liberty.common.faker.bbts.domain.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author ddavenport3
 */
public interface CardDao extends JpaRepository<Card, String> {
    
    public Card findByCardNumber(String cardNum);
    
    // shamelessly stolen from from ENVISION.CARD (I found it somewhere deep inside the bbts abyss)
    @Query(value = "select REGEXP_REPLACE(SYS_GUID(),'([A-F0-9]{8})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{12})','\\1-\\2-\\3-\\4-\\5') from dual", nativeQuery = true)
    public String getNewDomainId();
    
}
