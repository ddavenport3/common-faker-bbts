package edu.liberty.common.faker.bbts.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.log4j.Logger;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import edu.liberty.common.faker.core.FakePerson;
import edu.liberty.common.faker.bbts.FakeCustomer;
import edu.liberty.common.faker.bbts.FakerBbtsEnvironmentId;
import edu.liberty.common.faker.bbts.FakeCard;
import edu.liberty.common.faker.bbts.domain.Customer;
import edu.liberty.common.faker.bbts.repository.CustomerDao;
import static edu.liberty.common.faker.bbts.util.Util.luidToCardNumber;
import static edu.liberty.common.faker.bbts.util.Util.convertDateToBbtsDate;
import edu.liberty.common.faker.core.service.FakerService;

/**
 *
 * @author ddavenport3
 * 
 */
@Service
public class BbtsCustomerFakerServiceImpl extends FakerService implements BbtsCustomerFakerService {
    
    private static final Logger logger = Logger.getLogger(BbtsCustomerFakerServiceImpl.class);
    
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private BbtsCardFakerService cardService;  
    
    @Override
    @Transactional("bbtsTransactionManager")
    public void create(FakeCustomer fakeCustomer) {

        Customer customer = customerDao.findByCustomerId(fakeCustomer.getCustomerId());
        if (customer == null) {
            populateFakeCustomer(fakeCustomer);
            customer = populateCustomer(fakeCustomer);
            customer = customerDao.save(customer);
            flush();
            fakeCustomer.setCustomerId(customer.getCustomerId());  // automatically generated
            cardService.create(fakeCustomer);
        }
        
    }
    
    private Customer populateCustomer(FakeCustomer fakeCustomer) {
        Customer customer = new Customer(); 
        customer.setCardNumber(null);  
        customer.setCustomerNumber(fakeCustomer.getCustomerNumber());
        customer.setFirstName(fakeCustomer.getFirstName());
        customer.setMiddleName(fakeCustomer.getMiddleName());
        customer.setLastName(fakeCustomer.getLastName());
        customer.setBirthDate(fakeCustomer.getBirthDate()); 
        customer.setIsActive(fakeCustomer.getIsActive());
        customer.setLastModDateTime(12345.0);  // arbitrary bbts date
        customer.setDoorExtendedUnlockAllowed(fakeCustomer.getDoorExtendedUnlockAllowed());
        customer.setSex(fakeCustomer.getSex());
        customer.setPinNumber(fakeCustomer.getPinNumber());
        return customer;
    }
    
    private void populateFakeCustomer(FakeCustomer fakeCustomer) {
        
        FakePerson fakePerson = fakeCustomer.getFakePerson();
        
        fakeCustomer.setFakeCard(new FakeCard(fakeCustomer));  // make fake customer and fake card aware of each other
        fakeCustomer.setCardNumber(luidToCardNumber(fakePerson.getStudentId()));
        
        fakeCustomer.setFirstName(fakePerson.getFirstName());
        fakeCustomer.setMiddleName(fakePerson.getMiddle());
        fakeCustomer.setLastName(fakePerson.getLastName());
        
        if (fakeCustomer.getIsActive() ==  null) fakeCustomer.setIsActive("T");
        if (fakeCustomer.getDoorExtendedUnlockAllowed() ==  null) fakeCustomer.setDoorExtendedUnlockAllowed("F");
        if (fakeCustomer.getSex() ==  null) fakeCustomer.setSex("M");  
        if (fakeCustomer.getPinNumber() ==  null) fakeCustomer.setPinNumber(1234);
        if (fakeCustomer.getCustomerNumber() ==  null) fakeCustomer.setCustomerNumber(fakeCustomer.getCardNumber()); // must be a unique non-null string
        
        if (fakePerson.getBirthDate() ==  null) fakePerson.setBirthDate(new Date(System.currentTimeMillis())); // birthdate is a required field
        fakeCustomer.setBirthDate(convertDateToBbtsDate(fakePerson.getBirthDate()));

    }

    @Override
    public Customer findByCustomerId(Long customerId) {
        return customerDao.findByCustomerId(customerId);
    }
     
    @Override
    @PersistenceContext(unitName = "bbts")
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    @Override
    public String getEnvironmentId() {
        return FakerBbtsEnvironmentId.ENVIRONMENT_ID;
    }
    
}
