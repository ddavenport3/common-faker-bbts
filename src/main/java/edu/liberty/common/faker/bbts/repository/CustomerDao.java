package edu.liberty.common.faker.bbts.repository;

import edu.liberty.common.faker.bbts.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ddavenport3
 */
public interface CustomerDao extends JpaRepository<Customer, Integer> {
    
    public Customer findByCustomerId(Long customerId);
    
}
