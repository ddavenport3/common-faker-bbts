package edu.liberty.common.faker.bbts;

import edu.liberty.common.faker.core.FakeObject;
import java.util.Date;

/**
 *
 * @author ddavenport3
 */
public class FakeCard extends FakeObject {
    
    private String cardNumber = null;
    private String issueNumber;
    private Long customerId;
    private Integer cardType;
    private Integer cardStatus;
    private String cardStatusText;
    private Double cardStatusDatetime;
    private String lostFlag;
    private String domainId;
    private Integer issuerId; 
    private Date createdDateTime;
    private Date modifiedDateTime;
    private FakeCustomer fakeCustomer;
    
    public FakeCard(FakeCustomer fakeCustomer) {
        this.fakeCustomer = fakeCustomer;
    }

    public FakeCustomer getFakeCustomer() {
        return fakeCustomer;
    }

    public void setFakeCustomer(FakeCustomer fakeCustomer) {
        this.fakeCustomer = fakeCustomer;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getIssueNumber() {
        return issueNumber;
    }

    public void setIssueNumber(String issueNumber) {
        this.issueNumber = issueNumber;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Integer getCardType() {
        return cardType;
    }

    public void setCardType(Integer cardType) {
        this.cardType = cardType;
    }

    public Integer getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(Integer cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getCardStatusText() {
        return cardStatusText;
    }

    public void setCardStatusText(String cardStatusText) {
        this.cardStatusText = cardStatusText;
    }

    public Double getCardStatusDatetime() {
        return cardStatusDatetime;
    }

    public void setCardStatusDatetime(Double cardStatusDatetime) {
        this.cardStatusDatetime = cardStatusDatetime;
    }

    public String getLostFlag() {
        return lostFlag;
    }

    public void setLostFlag(String lostFlag) {
        this.lostFlag = lostFlag;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public Integer getIssuerId() {
        return issuerId;
    }

    public void setIssuerId(Integer issuerId) {
        this.issuerId = issuerId;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }
    
}

