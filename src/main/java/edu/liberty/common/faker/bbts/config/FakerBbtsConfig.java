package edu.liberty.common.faker.bbts.config;

import edu.liberty.common.faker.bbts.service.BbtsCustomerFakerService;
import edu.liberty.common.faker.bbts.service.BbtsCustomerFakerServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author whboyd
 * @author ddavenport3
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "bbtsEntityManagerFactory", 
        transactionManagerRef = "bbtsTransactionManager",
        basePackages = {"edu.liberty.common.faker.bbts"})
public class FakerBbtsConfig { 
        
    @Bean
    public BbtsCustomerFakerService fakerCustomerService() {
        return new BbtsCustomerFakerServiceImpl();
    }
    
}
