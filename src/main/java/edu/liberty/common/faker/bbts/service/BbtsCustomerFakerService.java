package edu.liberty.common.faker.bbts.service;

import edu.liberty.common.faker.bbts.FakeCustomer;
import edu.liberty.common.faker.bbts.domain.Customer;

/**
 *
 * @author ddavenport3
 */
public interface BbtsCustomerFakerService {
    
    public String getEnvironmentId();
    public void create(FakeCustomer fakeCustomer);  
    public Customer findByCustomerId(Long customerId);
    
}
