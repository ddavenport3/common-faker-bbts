package edu.liberty.common.faker.bbts.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author ddavenport3
 */
@Entity
@Table(name = "CARD", catalog = "", schema = "ENVISION")
public class Card implements Serializable {
    private static final long serialVersionUID = 1798070786993154676L;
    
    @Id
    @Basic(optional = false)
    @Column(name = "CARDNUM", nullable = false)
    private String cardNumber;
    
    @Column(name = "ISSUE_NUMBER")
    private String issueNumber;
    
    @Column(name="CUST_ID")
    private Long customerId;

    @Column(name = "CARD_TYPE", nullable = false)
    private Integer cardType;
	
    @Column(name = "CARD_STATUS", nullable = false)
    private Integer cardStatus;
                        	
    @Column(name = "CARD_STATUS_TEXT")
    private String cardStatusText;
 	
    @Column(name = "CARD_STATUS_DATETIME")
    private Double cardStatusDatetime;

    @Column(name = "LOST_FLAG", nullable = false)
    @Size(min = 1, max = 1)
    private String lostFlag = "F";
                        
    @Column(name = "CARD_IDM")
    private String cardIdm;
                        
    @Column(name = "DOMAINID", nullable = false)
    private String domainId;
                        
    @Column(name = "ISSUERID", nullable = false)
    private Integer issuerId ;
                        
    @Column(name = "CREATEDDATETIME", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDateTime;
                        
    @Column(name = "MODIFIEDDATETIME", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDateTime;
    
    @JoinColumn(name = "CUST_ID", referencedColumnName = "CUST_ID", insertable =  false, updatable = false)
    @OneToOne(optional = false)
    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getIssueNumber() {
        return issueNumber;
    }

    public void setIssueNumber(String issueNumber) {
        this.issueNumber = issueNumber;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Integer getCardType() {
        return cardType;
    }

    public void setCardType(Integer cardType) {
        this.cardType = cardType;
    }

    public Integer getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(Integer cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getCardStatusText() {
        return cardStatusText;
    }

    public void setCardStatusText(String cardStatusText) {
        this.cardStatusText = cardStatusText;
    }

    public Double getCardStatusDatetime() {
        return cardStatusDatetime;
    }

    public void setCardStatusDatetime(Double cardStatusDatetime) {
        this.cardStatusDatetime = cardStatusDatetime;
    }

    public String getLostFlag() {
        return lostFlag;
    }

    public void setLostFlag(String lostFlag) {
        this.lostFlag = lostFlag;
    }

    public String getCardIdm() {
        return cardIdm;
    }

    public void setCardIdm(String cardIdm) {
        this.cardIdm = cardIdm;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public Integer getIssuerId() {
        return issuerId;
    }

    public void setIssuerId(Integer issuerId) {
        this.issuerId = issuerId;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cardNumber != null ? cardNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Card)) {
            return false;
        }
        Card other = (Card) object;
        if (this.cardNumber == null || other.cardNumber == null || !this.cardNumber.equals(other.cardNumber)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.liberty.faker.repository.customer.generated.Card[ cardNumber=" + cardNumber + " ]";
    }

}
