package edu.liberty.common.faker.bbts;

import edu.liberty.common.faker.core.FakeObject;
import edu.liberty.common.faker.core.FakePerson;

/**
 *
 * @author ddavenport3
 * 
 */
public class FakeCustomer extends FakeObject {
    
    private Long customerId;
    private String customerNumber;
    private String defaultCardNumber;
    private String firstName;
    private String middleName;
    private String lastName;
    private Double birthDate; 
    private String sex;
    private Integer pinNumber;
    private String isActive;
    private Double activeStartDate;
    private Double activeEndDate;
    private Double openDateTime;
    private String doorExtendedUnlockAllowed;
    private FakePerson fakePerson;
    private FakeCard fakeCard;

    public FakeCard getFakeCard() {
        return fakeCard;
    }

    public void setFakeCard(FakeCard fakeCard) {
        this.fakeCard = fakeCard;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(double birthDate) {
        this.birthDate = birthDate;
    }

    public FakePerson getFakePerson() {
        return fakePerson;
    }

    public void setFakePerson(FakePerson fakePerson) {
        this.fakePerson = fakePerson;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCardNumber() {
        return defaultCardNumber;
    }

    public void setCardNumber(String defaultCardNumber) {
        this.defaultCardNumber = defaultCardNumber;
    }

    public double getCustomerBirthDate() {
        return birthDate;
    }

    public void setCustomerBirthDate(double birthDate) {
        this.birthDate = birthDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getPinNumber() {
        return pinNumber;
    }

    public void setPinNumber(Integer pinNumber) {
        this.pinNumber = pinNumber;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public Double getActiveStartDate() {
        return activeStartDate;
    }

    public void setActiveStartDate(Double activeStartDate) {
        this.activeStartDate = activeStartDate;
    }

    public Double getActiveEndDate() {
        return activeEndDate;
    }

    public void setActiveEndDate(Double activeEndDate) {
        this.activeEndDate = activeEndDate;
    }

    public Double getOpenDateTime() {
        return openDateTime;
    }

    public void setOpenDateTime(Double openDateTime) {
        this.openDateTime = openDateTime;
    }

    public String getDoorExtendedUnlockAllowed() {
        return doorExtendedUnlockAllowed;
    }

    public void setDoorExtendedUnlockAllowed(String doorExtendedUnlockAllowed) {
        this.doorExtendedUnlockAllowed = doorExtendedUnlockAllowed;
    }

}

