package edu.liberty.common.faker.bbts;

/** 
 * Created on Mar 25, 2015
 *
 * <p>Copyright(c) Liberty University®.  All Rights Reserved.
 * 
 * @author Paul Burton, <pburton@liberty.edu>
 * @author ddavenport3
 */

public class FakerBbtsEnvironmentId {
    
    public final static String ENVIRONMENT_ID = "bbts";

}