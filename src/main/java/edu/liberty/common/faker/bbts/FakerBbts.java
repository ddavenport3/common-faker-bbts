package edu.liberty.common.faker.bbts;

import edu.liberty.common.faker.bbts.service.BbtsCustomerFakerService;
import edu.liberty.common.faker.core.FakePerson;
import edu.liberty.common.faker.core.service.NamedFakerObjectService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 *
 * @author ddavenport3
 */
@Component
public class FakerBbts {
        
    static Logger logger = Logger.getLogger(edu.liberty.common.faker.core.Faker.class);
    
    @Autowired
    private NamedFakerObjectService namedFakerObjectService;
    @Autowired
    private BbtsCustomerFakerService customerFakerService;
    
    public FakeCustomer createCustomer(FakePerson fakePerson) {


        FakeCustomer fakeCustomer = new FakeCustomer();
        fakeCustomer.setFakePerson(fakePerson);
       
        customerFakerService.create(fakeCustomer);
        namedFakerObjectService.add(fakeCustomer.getFakeCard());
        namedFakerObjectService.add(fakeCustomer);
        return fakeCustomer;
        
    }
    
}
