package edu.liberty.common.faker.bbts.util;

import edu.liberty.luspringbase.util.DateUtil;
import java.util.Date;
import javax.management.timer.Timer;
import javax.xml.bind.TypeConstraintException;
import org.apache.log4j.Logger;


/**
 *
 * @author ddavenport3 (shamelessly stolen from the cards-bbts-test project with changes)
 * @author Tony Erskine
 * 
 * 
 */
public class Util extends DateUtil {
    
    private static final Logger logger = Logger.getLogger(Util.class);
    
    private static final String CUSTNUM_REGEX = "^0{14}\\d{8}$";
    private static final String LUID_REGEX = "^L\\d{8}$";
    private static final Date DEC_30_1899 = newDate(1899, 12, 30);
    
     /**
     * Parses the customer number to an LID.
     * If the customer number is not compatible with customer number format format throw exception
     * @param customerNumber
     * @return equivalent LID
     */
    public static String custNumToLuid(String customerNumber) {
        String luid;
        if(customerNumber == null || !customerNumber.matches(CUSTNUM_REGEX)) {
            throw new TypeConstraintException("A card number must start with 14 zeros followed by 8 numbers.");
        } else {
            // chop off the leading zeros and append an "L" in front
            luid = "L"+customerNumber.substring(customerNumber.length()-8);
            return luid;
        }
    }
    
    /**
     * Parses a customer number from an luid
     * If the luid is not compatible with luid format throw exception
     * @param luid
     * @return equivalent customer number
     */
    public static String luidToCardNumber(String luid) {
        if (luid == null || !luid.matches(LUID_REGEX)) {
            throw new TypeConstraintException("An luid must start with L followed by 8 numbers.");
        } else {
            // chop off the "L" on the studuent Id and pad the result with 14 zeros
            luid = luid.substring(1);
            return String.format("%022d", Integer.parseInt(luid));
        } 
    }

    /**
     * Tony: BBTS stores dates as floating point numbers that represent the number of days since December 30, 1899.
     * "Why that day," you might ask. Who the heck knows! I think they were shooting for January 1, 1900 or December 31,
     * 1899 and missed. 
     * 
     * Dan: Huzzah
     * 
     * @param dayCount
     * @return
     * 
     */
    public static Date convertBbtsDateToDate(Double dayCount) {
        if (dayCount == null) {
            throw new TypeConstraintException("Can't convert a null bbts date.");
        } else {
            // convert to millis
            long milliSeconds = (long) (dayCount * Timer.ONE_DAY);
            // add millis to Dec 30, 1899
            return new Date(DEC_30_1899.getTime() + milliSeconds);
        }
    }

    public static Double convertDateToBbtsDate(Date date) {
        if (date == null) {
            throw new TypeConstraintException("Can't convert a null date.");
        } else {
            // get number of millis since DEC_30_1899
            long millis = date.getTime() - DEC_30_1899.getTime();
            // get number of days (decimal) since DEC_30_1899
            return millis / (double) Timer.ONE_DAY;
        }
    }

}
