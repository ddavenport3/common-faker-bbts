package edu.liberty.common.faker.bbts.service;

import edu.liberty.common.faker.core.cleanup.service.DbCleanupService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author whboyd
 * @author ddavenport3
 * 
 */
@Service
public class BbtsDbCleanupService extends DbCleanupService {
    
    @PersistenceContext(unitName = "bbts")
    private EntityManager em;

    @Override
    @Transactional("bbtsTransactionManager")
    public void cleanupObject(Object entity) {
        super.doCleanupObject(entity, em);
    }

    @Override
    public boolean objectIsNew(Object entity) {
        return super.objectIsNew(entity, em);
    }
    
}
