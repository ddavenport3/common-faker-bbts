package edu.liberty.common.faker.bbts.service;

import edu.liberty.common.faker.bbts.domain.Card;
import edu.liberty.common.faker.bbts.FakeCustomer;

/**
 *
 * @author ddavenport3
 */
public interface BbtsCardFakerService {
    public String getEnvironmentId();
    public void create(FakeCustomer instance);  
    public Card findByCardNumber(String cardNum);
}

