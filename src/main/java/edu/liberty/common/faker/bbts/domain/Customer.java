package edu.liberty.common.faker.bbts.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author ddavenport3
 */
@Entity
@Table(name = "CUSTOMER", catalog = "", schema = "ENVISION")
@SequenceGenerator(name="customer_seq", sequenceName="envision.s_customer", allocationSize=1, initialValue=100)
public class Customer implements Serializable {
    private static final long serialVersionUID = -1798070786993154676L;
    
    @Id
    @Column(name = "CUST_ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_seq")
    private Long customerId;

    @Column(name = "CUSTNUM", nullable = false)
    private String customerNumber;
	
    @Column(name = "DEFAULTCARDNUM")
    private String defaultCardNumber;
    
    @Column(name = "FIRSTNAME")
    private String firstName;

    @Column(name = "MIDDLENAME")
    private String middleName;
            
    @Column(name = "LASTNAME")
    private String lastName;
    
    @Column(name = "BIRTHDATE")
    private Double birthDate; 

    @Column(name = "SEX")
    @Size(min = 1, max = 1)
    private String sex;

    @Column(name = "PINNUMBER")
    private Integer pinNumber;
    
    @Column(name = "IS_ACTIVE", nullable = false)
    private String isActive;
        
    @Column(name = "ACTIVE_START_DATE")
    private Double activeStartDate;
            
    @Column(name = "ACTIVE_END_DATE")
    private Double activeEndDate;
            
    @Column(name = "OPENDATETIME")
    private Double openDateTime;
            
    @Column(name = "LASTMOD_DATETIME", nullable = false)
    private Double lastModDateTime;

    @Column(name = "LASTMOD_USERNAME")
    private Double lastModUserName;
    
    @Column(name = "DOOREXTENDEDUNLOCKALLOWED", nullable = false)
    @Size(min = 1, max = 1)
    private String doorExtendedUnlockAllowed = "F";
    
    @JoinColumn(name = "DEFAULTCARDNUM", referencedColumnName = "CARDNUM", insertable =  false, updatable = false)
    @OneToOne(optional = false)
    private Card card;

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
                        
    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCardNumber() {
        return defaultCardNumber;
    }

    public void setCardNumber(String defaultCardNumber) {
        this.defaultCardNumber = defaultCardNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Double getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Double birthDate) {
        this.birthDate = birthDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getPinNumber() {
        return pinNumber;
    }

    public void setPinNumber(Integer pinNumber) {
        this.pinNumber = pinNumber;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public Double getActiveStartDate() {
        return activeStartDate;
    }

    public void setActiveStartDate(Double activeStartDate) {
        this.activeStartDate = activeStartDate;
    }

    public Double getActiveEndDate() {
        return activeEndDate;
    }

    public void setActiveEndDate(Double activeEndDate) {
        this.activeEndDate = activeEndDate;
    }

    public Double getOpenDateTime() {
        return openDateTime;
    }

    public void setOpenDateTime(Double openDateTime) {
        this.openDateTime = openDateTime;
    }

    public Double getLastModDateTime() {
        return lastModDateTime;
    }

    public void setLastModDateTime(Double lastModDateTime) {
        this.lastModDateTime = lastModDateTime;
    }

    public Double getLastModUserName() {
        return lastModUserName;
    }

    public void setLastModUserName(Double lastModUserName) {
        this.lastModUserName = lastModUserName;
    }

    public String getDoorExtendedUnlockAllowed() {
        return doorExtendedUnlockAllowed;
    }

    public void setDoorExtendedUnlockAllowed(String doorExtendedUnlockAllowed) {
        this.doorExtendedUnlockAllowed = doorExtendedUnlockAllowed;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (customerId != null ? customerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Customer)) {
            return false;
        }
        Customer other = (Customer) object;
        if (this.customerId == null || other.customerId == null || !this.customerId.equals(other.customerId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.liberty.faker.repository.person.generated.Customer[ customerId=" + customerId + " ]";
    }


}
