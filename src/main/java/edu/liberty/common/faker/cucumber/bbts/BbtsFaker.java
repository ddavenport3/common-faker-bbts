package edu.liberty.common.faker.cucumber.bbts;

import edu.liberty.common.faker.bbts.FakeCustomer;
import edu.liberty.common.faker.bbts.service.BbtsCustomerFakerService;
import edu.liberty.common.faker.core.FakePerson;
import edu.liberty.common.faker.core.service.NamedFakerObjectService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 *
 * @author ddavenport3
 */
@Component
public class BbtsFaker {
        
    static Logger logger = Logger.getLogger(edu.liberty.common.faker.core.Faker.class);
    
    @Autowired
    private NamedFakerObjectService namedFakerObjectService;
    @Autowired
    private BbtsCustomerFakerService customerFakerService;
    
    public FakeCustomer createCustomer(FakePerson fakePerson) {

        FakeCustomer fakeCustomer = new FakeCustomer();
        fakeCustomer.setFakePerson(fakePerson);
        customerFakerService.create(fakeCustomer);
        namedFakerObjectService.add(fakeCustomer);  // register fakeCustomer
        namedFakerObjectService.add(fakeCustomer.getFakeCard()); // register fakeCard
        return fakeCustomer;
        
    }
    
}
