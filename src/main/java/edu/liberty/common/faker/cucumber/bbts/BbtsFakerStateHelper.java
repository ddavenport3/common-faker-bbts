package edu.liberty.common.faker.cucumber.bbts;

import edu.liberty.common.faker.bbts.FakeCustomer;
import edu.liberty.common.faker.bbts.FakeCard;
import org.springframework.stereotype.Component;
import org.springframework.test.annotation.DirtiesContext;

/**
 *
 * @author ddavenport3
 */
@DirtiesContext
@Component
public class BbtsFakerStateHelper {
    
    private FakeCustomer lastCreatedCustomer;
    private FakeCard lastCreatedCard;

    public FakeCustomer getLastCreatedCustomer() {
        return lastCreatedCustomer;
    }

    public void setLastCreatedCustomer(FakeCustomer lastCreatedCustomer) {
        this.lastCreatedCustomer = lastCreatedCustomer;
    }
    
    public FakeCard getLastCreatedCard() {
        return lastCreatedCard;
    }

    public void setLastCreatedCard(FakeCard lastCreatedCard) {
        this.lastCreatedCard = lastCreatedCard;
    }

}
