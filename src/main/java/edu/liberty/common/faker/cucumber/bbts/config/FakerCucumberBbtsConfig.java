package edu.liberty.common.faker.cucumber.bbts.config;

import edu.liberty.common.faker.core.cleanup.aspect.FakerCleanupAspect;
import edu.liberty.common.faker.core.cleanup.service.CleanupService;
import edu.liberty.common.faker.core.cleanup.service.CleanupServiceImpl;
import edu.liberty.common.faker.cucumber.bbts.BbtsFakerStateHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 *
 * @author whboyd
 */
@Configuration
@EnableAspectJAutoProxy
public class FakerCucumberBbtsConfig {
    
    @Bean
    public CleanupService cleanupService() {
        return new CleanupServiceImpl();
    }

    @Bean
    public FakerCleanupAspect fakerCleanupAspect() {
        return new FakerCleanupAspect();
    }

}
